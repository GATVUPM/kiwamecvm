from flask import Flask, request, Response
import jsonpickle
import numpy as np
from PIL import Image
import cv2
import os
import multiprocessing
import random

import torch
import torchvision
from torchvision import models, transforms, datasets
from torch.utils.data import DataLoader
from torch import nn
from torch import optim
from torch.autograd import Variable

from detectron2.engine import DefaultPredictor
from detectron2.config import get_cfg
from detectron2.utils.visualizer import Visualizer
from detectron2.utils.visualizer import ColorMode
from detectron2.data import DatasetCatalog, MetadataCatalog

app = Flask(__name__)

# Init networks
f = open("Datasets/food-101/meta/labels.txt", "r")
labels = f.read().split("\n")
device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
gpu = True if torch.cuda.is_available() else False

model_imgnet = models.wide_resnet101_2(pretrained=True) # model
num_ftrs = model_imgnet.fc.in_features
model_imgnet.fc = nn.Linear(in_features=num_ftrs, out_features=101, bias=True)
model = model_imgnet.to(device)
model.eval()

MODEL_PATH = 'model_data/'
model.load_state_dict(torch.load(MODEL_PATH+"stage2_35.pth")) 

imagenet_stats = [(0.485, 0.456, 0.406), (0.229, 0.224, 0.225)]
valid_tfms = transforms.Compose([transforms.CenterCrop(224), transforms.ToTensor(), transforms.Normalize(imagenet_stats[0], imagenet_stats[1])])


food_metadata = MetadataCatalog.get("foodSeg/train")
cfg = get_cfg()
cfg.merge_from_file("detectron2_repo/configs/COCO-InstanceSegmentation/mask_rcnn_R_50_FPN_3x.yaml")
cfg.MODEL.ROI_HEADS.NUM_CLASSES = 1  # only has one class (ballon)
cfg.MODEL.WEIGHTS = os.path.join(cfg.OUTPUT_DIR, "model_final.pth")
mask_predictor = DefaultPredictor(cfg)


# Predcitions based on food image
def predict_food(img):
    img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
    im_pil = Image.fromarray(img)
    input_img = valid_tfms(im_pil)
    if gpu :
        input_img = input_img.unsqueeze(0).cuda() 
    else :
        input_img = input_img.unsqueeze(0).cpu()
    var_ip = Variable(input_img)
    output = model(var_ip)
    _, pred = torch.max(output, 1)
    return labels[int(pred[0])]


def predcit_mask(img):
    outputs = mask_predictor(img)
    v = Visualizer(img[:, :, ::-1], metadata=food_metadata, scale=0.8, instance_mode=ColorMode.IMAGE_BW)
    v = v.draw_instance_predictions(outputs["instances"].to("cpu"))
    #cv2.imshow(str(random.uniform(0, 1)), v.get_image()[:, :, ::-1])
    #cv2.waitKey(1000)
    #cv2.destroyAllWindows()


# route http posts to this method
@app.route('/food_prediction', methods=['POST'])
def test():

    r = request
    # convert string of image data to uint8
    nparr = np.fromstring(r.data, np.uint8)
    # decode image
    img = cv2.imdecode(nparr, cv2.IMREAD_COLOR)

    food = predict_food(img)
    print(food)
    predcit_mask(img)

    response = {'message': 'The food is: ' + food}
    # encode response using jsonpickle
    response_pickled = jsonpickle.encode(response)

    return Response(response=response_pickled, status=200, mimetype="application/json")

if __name__ == '__main__':

    app.run()