import argparse
import torch
import cv2
import os
import torch.nn.parallel
import volume_net.modules as modules
import volume_net.net as net
import volume_net.resnet as  resnet
import volume_net.densenet as densenet
import volume_net.senet as senet
from volume_net.demo_transform import *

import numpy as np

import matplotlib.image
import matplotlib.pyplot as plt

from torchvision import transforms
from PIL import Image


def define_model(is_resnet, is_densenet, is_senet):
    if is_resnet:
        original_model = resnet.resnet50(pretrained = True)
        Encoder = modules.E_resnet(original_model) 
        model = net.model(Encoder, num_features=2048, block_channel = [256, 512, 1024, 2048])
    if is_densenet:
        original_model = densenet.densenet161(pretrained=True)
        Encoder = modules.E_densenet(original_model)
        model = net.model(Encoder, num_features=2208, block_channel = [192, 384, 1056, 2208])
    if is_senet:
        original_model = senet.senet154(pretrained='imagenet')
        Encoder = modules.E_senet(original_model)
        model = net.model(Encoder, num_features=2048, block_channel = [256, 512, 1024, 2048])

    return model


def test(nyu2_loader, model, width, height):    
    vol = get_volume(out_grey, args.json)
    print("Volume:")
    print(vol)
    print("unit: cm^3")
    out_file = open(os.path.join(args.output, "out.txt"), "w")
    out_file.write("Volume:\n")
    out_file.write(str(vol))
    out_file.write("\n")
    out_file.write("unit: cm^3")
    out_file.close()
    get_mask(out_grey, args.json)
        

if __name__ == '__main__':

    path_img = "test_img.jpg"

    transform = transforms.Compose([Scale([320, 240]), CenterCrop([304, 228]), ToTensor(), Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225])])
    image = transform(Image.open(path_img))

    img = cv2.imread(path_img)
    height, width = img.shape[:2]
    
    model = define_model(is_resnet=False, is_densenet=False, is_senet=True)
    model = torch.nn.DataParallel(model).cuda()
    model.load_state_dict(torch.load('volume_net/pretrained_model/model_senet'))
    model.eval()

    image = torch.autograd.Variable(image.unsqueeze(0), volatile=True).cuda()
    out = model(image)
    out = out.view(out.size(2),out.size(3)).data.cpu().numpy()
    max_pix = out.max()
    min_pix = out.min()
    out = (out-min_pix)/(max_pix-min_pix)*255
    out = cv2.resize(out,(width,height),interpolation=cv2.INTER_CUBIC).astype(np.uint8)
    out = cv2.merge((out, out, out))
    cv2.imshow("Grey depth", out)
    cv2.waitKey(0)
    cv2.imwrite("out_grey.png",out)
    out_grey = cv2.imread("out_grey.png",0)
    out_color = cv2.applyColorMap(out_grey, cv2.COLORMAP_JET)
    cv2.imwrite("out_color.png",out_color)
    