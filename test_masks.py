import os
import numpy as np
import json
from detectron2.structures import BoxMode
from detectron2.engine import DefaultTrainer
from detectron2.engine import DefaultPredictor
from detectron2.config import get_cfg
from detectron2.data import DatasetCatalog, MetadataCatalog
from detectron2.utils.visualizer import Visualizer
from detectron2.utils.visualizer import ColorMode
import itertools
import cv2
import random

# write a function that loads the dataset into detectron2's standard format
def get_food_dicts(img_dir):
    json_file = os.path.join(img_dir, "json_masks.json")
    with open(json_file) as f:
        imgs_anns = json.load(f)["data"]

    dataset_dicts = []
    for v in imgs_anns:
        record = {}


        filename = os.path.join(img_dir, v["file_name"])

        try:
            height, width = cv2.imread(filename).shape[:2]
        except:
            continue

        if height>width:
            continue
        
        record["file_name"] = filename
        record["height"] = height
        record["width"] = width
      
        annos = v["anno_x"]
        objs = []
        for i, anno in enumerate(annos):

            px = anno
            py = v["anno_y"][i]
            poly = [(x + 0.5, y + 0.5) for x, y in zip(px, py)]
            poly = list(itertools.chain.from_iterable(poly))

            obj = {
                "bbox": v["bbox"][i],
                "bbox_mode": BoxMode.XYXY_ABS,
                "segmentation": [poly],
                "category_id": 0,
                "iscrowd": 0
            }
            objs.append(obj)
        record["annotations"] = objs
        dataset_dicts.append(record)
    return dataset_dicts

food_metadata = MetadataCatalog.get("foodSeg/train").set(thing_classes=["foodSeg"])

cfg = get_cfg()
cfg.merge_from_file("detectron2_repo/configs/COCO-InstanceSegmentation/mask_rcnn_R_101_FPN_3x.yaml")
cfg.MODEL.ROI_HEADS.NUM_CLASSES = 1
cfg.MODEL.WEIGHTS = os.path.join(cfg.OUTPUT_DIR, "model_0004999.pth")
cfg.MODEL.ROI_HEADS.SCORE_THRESH_TEST = 0.7
cfg.DATASETS.TEST = ("foodSeg/val", )
predictor = DefaultPredictor(cfg)

dataset_dicts = get_food_dicts("foodSeg/val")

for d in random.sample(dataset_dicts, 30):    
    im = cv2.imread(d["file_name"])
    outputs = predictor(im)
    v = Visualizer(im[:, :, ::-1],
                   metadata=food_metadata, 
                   scale=0.8, 
                   instance_mode=ColorMode.IMAGE_BW   # remove the colors of unsegmented pixels
    )
    v = v.draw_instance_predictions(outputs["instances"].to("cpu"))
    cv2.imshow("Img", v.get_image()[:, :, ::-1])
    cv2.waitKey(0)