# Clone repository KiwameCVM
# Open a terminal and go to the cloned folder

# Create virtual environment in anaconda
conda create -n food_server python=3.6
conda activate food_server

# Install libraries
conda install pytorch==1.3.0 torchvision==0.4.1 cudatoolkit=10.0 -c pytorch  # or cudatoolkit=10.1 depending on cuda version
pip install git+https://github.com/facebookresearch/fvcore.git
pip install Pillow==6.1
pip install flask
pip install jsonpickle
pip install pycocotools
pip install opencv-python
pip install opencv-contrib-python
pip install -e detectron2_repo
