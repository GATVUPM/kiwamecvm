from flask import Flask, request, Response, jsonify
import jsonpickle
import numpy as np
from PIL import Image
import cv2
import os
import multiprocessing
import random
import os, sys
import json

from detectron2.structures import BoxMode
from detectron2.engine import DefaultPredictor
from detectron2.config import get_cfg
from detectron2.data import MetadataCatalog
from detectron2.utils.visualizer import Visualizer
from detectron2.utils.visualizer import ColorMode

import torch
import torchvision
from torchvision import models, transforms, datasets
from torch.utils.data import DataLoader
from torch import nn
from torch import optim
from torch.autograd import Variable
import torch.nn.parallel

import volume_net.modules as modules
import volume_net.net as net
import volume_net.resnet as  resnet
import volume_net.densenet as densenet
import volume_net.senet as senet
from volume_net.demo_transform import *
from volume_net.volume import *

import matplotlib.image
import matplotlib.pyplot as plt
from torchvision import transforms
from PIL import Image
import PIL.ExifTags
from ast import literal_eval

from shapely.ops import cascaded_union, unary_union
from shapely.geometry import Polygon

import geopandas as gpd

from flasgger import Swagger
from werkzeug.utils import secure_filename
from flasgger.utils import swag_from



call_volume_estimation = {
  "parameters": [
    {
      "name": "image",
      "in": "formData",
      "type": "file",
      "required": "true",
    }
  ],
  "responses": {
    "200": {
      "description": "Estimated Volume of each ingredient in the food image",
      "content":{
        "application/json":{
          "schema":{
            "$ref":"#/components/schemas/response"
          }
        }
      }
    }
  }
}


call_dataset_contribution = {
  "parameters": [
    {
      "name": "image",
      "in": "formData",
      "type": "file",
      "required": "true",
    },
    {
      "name": "annotation",
      "in": "formData",
      "type": "string",
      "required": "true",
    }
  ],
  "responses": {
    "200": {
      "description": "Thanks to contribute!",
      "examples": {
        "rgb": [
          "red",
          "green",
          "blue"
        ]
      }
    }
  }
}


app = Flask(__name__)
swagger = Swagger(app)


def define_model(is_resnet, is_densenet, is_senet):
    if is_resnet:
        original_model = resnet.resnet50(pretrained = True)
        Encoder = modules.E_resnet(original_model) 
        model = net.model(Encoder, num_features=2048, block_channel = [256, 512, 1024, 2048])
    if is_densenet:
        original_model = densenet.densenet161(pretrained=True)
        Encoder = modules.E_densenet(original_model)
        model = net.model(Encoder, num_features=2208, block_channel = [192, 384, 1056, 2208])
    if is_senet:
        original_model = senet.senet154(pretrained='imagenet')
        Encoder = modules.E_senet(original_model)
        model = net.model(Encoder, num_features=2048, block_channel = [256, 512, 1024, 2048])

    return model


def mask_to_polygons(mask):
    mask = np.ascontiguousarray(mask)
    res = cv2.findContours(mask.astype("uint8"), cv2.RETR_CCOMP, cv2.CHAIN_APPROX_NONE)
    hierarchy = res[-1]
    if hierarchy is None:  # empty mask
        return [], False
    has_holes = (hierarchy.reshape(-1, 4)[:, 3] >= 0).sum() > 0
    res = res[-2]
    res = [x.flatten() for x in res]
    res = [x for x in res if len(x) >= 6]
    return res, has_holes


def convert_masks_annotations(points):
    points_x = points[::2]
    points_y = points[1::2]

    anno_format = []
    for i in range(points_x.shape[0]):
        anno_format.append([int(points_x[i]), int(points_y[i])])

    return np.array(anno_format)


def get_plate_mask(masks):

    polys = []

    x_maxi = 0
    x_mini = sys.maxsize
    y_maxi = 0
    y_mini = sys.maxsize
    for mask in masks:

        points_x = mask[::2]
        points_y = mask[1::2]

        anno_format = []
        for i in range(points_x.shape[0]):
            anno_format.append((points_x[i], points_y[i]))

            if points_x[i]>x_maxi:
                x_maxi = points_x[i]
            if points_y[i]>y_maxi:
                y_maxi = points_y[i]
            if points_x[i]<x_mini:
                x_mini = points_x[i]
            if points_y[i]<y_mini:
                y_mini = points_y[i]

        polys.append(Polygon(anno_format))

    print((x_maxi-x_mini)*30/4.7)

    try:
        x, y = cascaded_union(polys).exterior.coords.xy
    except:
        x = [x_mini, x_maxi, x_maxi, x_mini]
        y = [y_mini, y_mini, y_maxi, y_maxi]
        print(x, y)

    anno_format = []
    for i in range(len(x)):
        anno_format.append([int(x[i]), int(y[i])])
        
    #boundary = gpd.GeoSeries(cascaded_union(polys))
    #boundary.plot(color = 'red')
    #plt.show()

    return anno_format


 # Load detection masks network
food_metadata = MetadataCatalog.get("foodSeg/train").set(thing_classes=["foodSeg"])
cfg = get_cfg()
cfg.merge_from_file("detectron2_repo/configs/COCO-InstanceSegmentation/mask_rcnn_R_101_FPN_3x.yaml")
cfg.MODEL.ROI_HEADS.NUM_CLASSES = 1
cfg.MODEL.WEIGHTS = "model_masks_5000.pth"
cfg.MODEL.ROI_HEADS.SCORE_THRESH_TEST = 0.7
cfg.DATASETS.TEST = ("foodSeg/val", )
predictor = DefaultPredictor(cfg)


# Loas depth network
model_depth = define_model(is_resnet=False, is_densenet=False, is_senet=True)
model_depth = torch.nn.DataParallel(model_depth).cuda()
model_depth.load_state_dict(torch.load('volume_net/pretrained_model/model_senet'))
model_depth.eval()


# Load recognition network
f = open("labels.txt", "r")
labels = f.read().split("\n")

device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
gpu = True if torch.cuda.is_available() else False

model_imgnet = models.wide_resnet101_2(pretrained=True) # model
num_ftrs = model_imgnet.fc.in_features
model_imgnet.fc = nn.Linear(in_features=num_ftrs, out_features=101, bias=True)
model_rec = model_imgnet.cuda()
model_rec.eval()

MODEL_PATH = 'model_data/'
model_rec.load_state_dict(torch.load(MODEL_PATH+"stage2_35.pth")) 

imagenet_stats = [(0.485, 0.456, 0.406), (0.229, 0.224, 0.225)]
valid_tfms = transforms.Compose([transforms.CenterCrop(224), transforms.ToTensor(), transforms.Normalize(imagenet_stats[0], imagenet_stats[1])])


def predict_food(img):

    height, width = img.shape[:2]

    try:
        exif = {PIL.ExifTags.TAGS[k]: v for k, v in Image.open(path_img)._getexif().items() if k in PIL.ExifTags.TAGS}
        print(exif)
        f1, f2 = exif["FocalLength"]
        focal_length = float(f1)/float(f2)
    except:
        focal_length = 5.0

    print("Focal Length: " + str(focal_length) + " mm")

    # Predict masks and bboxes
    outputs = predictor(img)
    #v = Visualizer(img[:, :, ::-1], metadata=food_metadata, scale=0.8, instance_mode=ColorMode.IMAGE_BW)
    #v = v.draw_instance_predictions(outputs["instances"].to("cpu"))
    #cv2.imshow("Img masks", v.get_image()[:, :, ::-1])
    #cv2.waitKey(0)

    predictions = outputs["instances"].to("cpu")
    bboxes = predictions.pred_boxes if predictions.has("pred_boxes") else None
    bboxes = bboxes.tensor.numpy()
    scores = predictions.scores if predictions.has("scores") else None
    scores = scores.numpy()
    masks = np.asarray(predictions.pred_masks)
    masks = [mask_to_polygons(x)[0][0] for x in masks]
    #print(bboxes, scores, masks)

    # Get complete plate mask
    plate_mask = get_plate_mask(masks)
    json_data = {"shapes": [{"label": "plate", "points": plate_mask}]}

    for i, box in enumerate(bboxes):

        mask_pp_format = convert_masks_annotations(masks[i])

        img_rec = img[int(box[1]):int(box[3]), int(box[0]):int(box[2])]

        img_rec = cv2.cvtColor(img_rec, cv2.COLOR_BGR2RGB)
        im_pil = Image.fromarray(img_rec)
        input_img = valid_tfms(im_pil)

        if gpu :
            input_img = input_img.unsqueeze(0).cuda() 
        else :
            input_img = input_img.unsqueeze(0).cpu()

        var_ip = Variable(input_img)
        output = model_rec(var_ip)

        _, pred = torch.max(output, 1)
        food_rec = labels[int(pred[0])]

        json_data["shapes"].append({"label": food_rec, "points": mask_pp_format})


    # Predict depth
    transform = transforms.Compose([Scale([320, 240]), CenterCrop([304, 228]), ToTensor(), Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225])])
    
    image = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
    im_pil = Image.fromarray(image)

    image = transform(im_pil)
    with torch.no_grad():
        image = image.unsqueeze(0).cuda()
        out = model_depth(image)
        out = out.view(out.size(2),out.size(3)).data.cpu().numpy()
        max_pix = out.max()
        min_pix = out.min()
        out = (out-min_pix)/(max_pix-min_pix)*255
        out = cv2.resize(out,(width,height),interpolation=cv2.INTER_CUBIC).astype(np.uint8)
        out3 = cv2.merge((out, out, out))
        #cv2.imshow("Grey depth", out3)
        #out_color = cv2.applyColorMap(out, cv2.COLORMAP_JET)


    json_vol = get_volume(out, json_data)

    print(json_vol)

    return json_vol


UPLOAD_FOLDER = 'uploads/'
DATASET_FOLDER = "kiwame_dataset"
ALLOWED_EXTENSIONS = set(['txt', 'pdf', 'png', 'jpg', 'jpeg', 'gif'])

def allowed_file(filename):
    return '.' in filename and \
       filename.rsplit('.', 1)[1] in ALLOWED_EXTENSIONS


# route http posts to this method
@app.route('/volume_estimation/', methods=['POST'])
@swag_from(call_volume_estimation)
def volume_estimation():
    #"""
    #file: yml_docs/volume.yml
    #"""

    #print(file = request.files['image'])

    '''file = request.files['image']
    if file and allowed_file(file.filename):
        filename = secure_filename(file.filename)
        file.save(os.path.join(UPLOAD_FOLDER, filename))'''

    filestr = request.files['image'].read()
    #convert string data to numpy array
    npimg = np.fromstring(filestr, np.uint8)
    # convert numpy array to image
    img = cv2.imdecode(npimg, cv2.IMREAD_COLOR)

    #r = request
    # convert string of image data to uint8
    #nparr = np.fromstring(file, np.uint8)

    # decode image
    #img = cv2.imdecode(nparr, cv2.IMREAD_COLOR)

    #img = cv2.imread(os.path.join(UPLOAD_FOLDER, filename))

    response = predict_food(img)

    # encode response using jsonpickle
    response_pickled = jsonpickle.encode(response)

    return Response(response=response_pickled, status=200, mimetype="application/json")


# route http posts to this method
@app.route('/contribute_to_database/', methods=['POST'])
@swag_from(call_dataset_contribution)
def store_contribution():
    #"""
    #file: yml_docs/database.yml
    #"""

    #print(file = request.files['image'])

    '''file = request.files['image']
    if file and allowed_file(file.filename):
        filename = secure_filename(file.filename)
        file.save(os.path.join(UPLOAD_FOLDER, filename))'''

    filestr = request.files['image'].read()
    #convert string data to numpy array
    npimg = np.fromstring(filestr, np.uint8)
    # convert numpy array to image
    img = cv2.imdecode(npimg, cv2.IMREAD_COLOR)

    cv2.imwrite(os.path.join(DATASET_FOLDER, "images", request.files['image'].filename), img)

    anno = request.form["annotation"]
    text_file = open(os.path.join(DATASET_FOLDER, "annotations", request.files['image'].filename.split(".")[0]+".txt"), "w")
    text_file.write(anno)
    text_file.close()

    #r = request
    # convert string of image data to uint8
    #nparr = np.fromstring(file, np.uint8)

    # decode image
    #img = cv2.imdecode(nparr, cv2.IMREAD_COLOR)

    #img = cv2.imread(os.path.join(UPLOAD_FOLDER, filename))

    # encode response using jsonpickle
    response = {"meassage": "Stored! Thank you."}
    response_pickled = jsonpickle.encode(response)

    #response_pickled = jsonpickle.encode(response)

    return Response(response=response_pickled, status=200, mimetype="application/json")


@app.route('/spec/<palette>/')
def colors(palette):
    """
    file: yml_docs/colors.yml
    """
    all_colors = {
        'cmyk': ['cian', 'magenta', 'yellow', 'black'],
        'rgb': ['red', 'green', 'blue']
    }
    if palette == 'all':
        result = all_colors
    else:
        result = {palette: all_colors.get(palette)}

    return jsonify(result)


if __name__ == '__main__':

    app.run(debug=True)