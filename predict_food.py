import torch
import torchvision
from torchvision import models, transforms, datasets
from torch.utils.data import DataLoader
from torch import nn
from torch import optim
from torch.autograd import Variable

import os
import numpy as np
import matplotlib.pyplot as plt
from PIL import Image
import cv2

f = open("Datasets/food-101/meta/labels.txt", "r")
labels = f.read().split("\n")
device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
gpu = True if torch.cuda.is_available() else False

model_imgnet = models.wide_resnet101_2(pretrained=True) # model
num_ftrs = model_imgnet.fc.in_features
model_imgnet.fc = nn.Linear(in_features=num_ftrs, out_features=101, bias=True)
model = model_imgnet.to(device)
model.eval()

MODEL_PATH = 'model_data/'
model.load_state_dict(torch.load(MODEL_PATH+"stage2_35.pth")) 

imagenet_stats = [(0.485, 0.456, 0.406), (0.229, 0.224, 0.225)]
valid_tfms = transforms.Compose([transforms.CenterCrop(224), transforms.ToTensor(), transforms.Normalize(imagenet_stats[0], imagenet_stats[1])])




img = cv2.imread("test_img.jpg")
img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
im_pil = Image.fromarray(img)
input_img = valid_tfms(im_pil)

if gpu :
    input_img = input_img.unsqueeze(0).cuda() 
else :
    input_img = input_img.unsqueeze(0).cpu()

var_ip = Variable(input_img)
output = model(var_ip)

_, pred = torch.max(output, 1)
print(labels[int(pred[0])])