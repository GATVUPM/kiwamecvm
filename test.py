import json
import numpy as np

json_clean = {"data": []}

json_file = "annotations.json"
with open(json_file) as f:
    imgs_anns = json.load(f)

    print(len(imgs_anns["annotations"]), len(imgs_anns["categories"]), len(imgs_anns["images"]))

    for data in imgs_anns["images"]:

        w = data["width"]
        h = data["height"]
        img_name = data["file_name"]

        annotations_x = []
        annotations_y = []
        boxes = []

        for data_ann in imgs_anns["annotations"]:
            if data["id"] == data_ann["image_id"]:
                anno = np.array(data_ann["segmentation"][0])
                annotations_x.append(anno[0::2].tolist())
                annotations_y.append(anno[1::2].tolist())
                boxes.append([np.min(anno[0::2]), np.min(anno[1::2]), np.max(anno[0::2]), np.max(anno[1::2])])

        json_clean["data"].append({"file_name": img_name, "width": w, "height": h, "anno_x": annotations_x, "anno_y": annotations_y, "bbox": boxes})

with open('json_masks.json', 'w') as outfile:
    json.dump(json_clean, outfile)