#Classification of Food-101 datasets

import torch
import torchvision
from torchvision import models, transforms, datasets
from torch.utils.data import DataLoader
from torch import nn
from torch import optim
from torch.autograd import Variable

import os
import numpy as np
import matplotlib.pyplot as plt
from collections import defaultdict
import collections
from shutil import copy
from shutil import copytree, rmtree
import random
from tqdm import tqdm_notebook as tqdm
import math
import time

#No. of food categories (classes): 101
#Total no. of images: 101,000 (1000 images/class)
#Training images/class: 750
#Test images/class: 250
#Rescaled Image size (maximum): (512x512) pixels
#N.B: The training images were not cleaned i.e. contain some amount of noise (like intense colors or wrong labels)

#torch.multiprocessing.freeze_support()

bs = 64
epochs = 3
device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
imagenet_stats = [(0.485, 0.456, 0.406), (0.229, 0.224, 0.225)]

FOOD_PATH = "./Datasets/food-101"
IMG_PATH = FOOD_PATH+"/images/"
META_PATH = FOOD_PATH+"/meta/"
TRAIN_PATH = FOOD_PATH+"/train/"
VALID_PATH = FOOD_PATH+"/valid/"
MODEL_PATH = 'model_data/'

gpu = True if torch.cuda.is_available() else False

filename = MODEL_PATH+'clr.pth'

# Helper functions
def pp_(*args, n_dash=120):
    for arg in args:
        print(arg)
        print("-"*n_dash)

def list_dir(path="./"):
	return os.listdir(path)

def cal_mean_std(train_data):
    return np.mean(train_data, axis=(0,1,2))/255, np.std(train_data, axis=(0,1,2))/255

def save_checkpoint(model, is_best, filename='model_data/checkpoint.pth'):
    """Save checkpoint if a new best is achieved"""
    if is_best:
        torch.save(model.state_dict(), filename)  # save checkpoint
    else:
        print ("=> Validation Accuracy did not improve")
        
# from fastai library
def load_checkpoint(model, filename = 'model_data/checkpoint.pth'):
    sd = torch.load(filename, map_location=lambda storage, loc: storage)
    names = set(model.state_dict().keys())
    for n in list(sd.keys()): 
        if n not in names and n+'_raw' in names:
            if n+'_raw' not in sd: sd[n+'_raw'] = sd[n]
            del sd[n]
    model.load_state_dict(sd)    

def save_model(model, path):
    torch.save(model.state_dict(), path)
    
def load_model(model, path):
    model.load_state_dict(torch.load(path))  

def calc_iters(dataset, num_epochs, bs):
    return int(len(dataset) * num_epochs /bs)

def accuracy(output, target, is_test=False):
    global total
    global correct
    batch_size = output.shape[0]
    total += batch_size
    
    _, pred = torch.max(output, 1)
    if is_test:
        preds.extend(pred)
    correct += (pred == target).sum()
    return 100 * correct / total

class AvgStats(object):
    def __init__(self):
        self.reset()
        
    def reset(self):
        self.losses =[]
        self.precs =[]
        self.its = []
        
    def append(self, loss, prec, it):
        self.losses.append(loss)
        self.precs.append(prec)
        self.its.append(it)

def freeze(model):
    child_counter = 0
    for name, child in model.named_children():
        if child_counter < 7:
            print("name ",name, "child ",child_counter," was frozen")
            for param in child.parameters():
                param.requires_grad = False
        elif child_counter == 7:
            children_of_child_counter = 0
            for children_of_child in child.children():
                if children_of_child_counter < 2:
                    for param in children_of_child.parameters():
                        param.requires_grad = False
                    print("name ",name, 'child ', children_of_child_counter, 'of child',child_counter,' was frozen')
                else:
                    print("name ",name, 'child ', children_of_child_counter, 'of child',child_counter,' was not frozen')
                children_of_child_counter += 1

        else:
            print("name ",name, "child ",child_counter," was not frozen")
        child_counter += 1

def unfreeze(model):
    for param in model.parameters():
        param.requires_grad = True

def print_frozen_state(model):
    child_counter = 0
    for name, child in model.named_children():
        for param in child.parameters():
            if param.requires_grad == True:
                print("child ",child_counter,"named:",name," is unfrezed")
            elif param.requires_grad == False:
                print("child ",child_counter,"named:",name," is frezed")
        child_counter += 1

def update_lr(optimizer, lr):
    for g in optimizer.param_groups:
        g['lr'] = lr

def update_mom(optimizer, mom):
    for g in optimizer.param_groups:
        g['momentum'] = mom

# Dataset Preparation. Create a class to download & prepare train and validation data.
class FOOD101():
    def __init__(self):
        self.train_ds, self.valid_ds, self.train_cls, self.valid_cls = [None]*4
        self.imgenet_mean = imagenet_stats[0]
        self.imgenet_std = imagenet_stats[1]
   
    def get_data_extract(self):
        if "food-101" in os.listdir():
            print("Dataset already exists")
        else:
            print("Downloading the data...")
            #!wget http://data.vision.ee.ethz.ch/cvl/food-101.tar.gz
            print("Dataset downloaded!")
            print("Extracting data..")
            #!tar xzvf food-101.tar.gz
            print("Extraction done!")
        
    def _get_tfms(self):
        train_tfms = transforms.Compose([
            transforms.RandomResizedCrop(224),
            transforms.RandomHorizontalFlip(), 
            transforms.ToTensor(),
            transforms.Normalize(self.imgenet_mean, self.imgenet_std)])
        
        valid_tfms = transforms.Compose([
            transforms.CenterCrop(224),
            transforms.ToTensor(),
            transforms.Normalize(self.imgenet_mean, self.imgenet_std)])        
        return train_tfms, valid_tfms            
            
    def get_dataset(self,root_dir='./food-101/'):
        train_tfms, valid_tfms = self._get_tfms() # transformations
        self.train_ds = datasets.ImageFolder(root=TRAIN_PATH, transform=train_tfms)
        self.valid_ds = datasets.ImageFolder(root=VALID_PATH, transform=valid_tfms)        
        self.train_classes = self.train_ds.classes
        self.valid_classes = self.valid_ds.classes

        assert self.train_classes==self.valid_classes
        return self.train_ds, self.valid_ds, self.train_classes

    
    def get_dls(self, train_ds, valid_ds, bs, **kwargs):
        return (DataLoader(train_ds, batch_size=bs, shuffle=True, **kwargs),
               DataLoader(valid_ds, batch_size=bs//2, shuffle=False, **kwargs))
    
food = FOOD101()
food.get_data_extract()
pp_(list_dir(FOOD_PATH), list_dir(IMG_PATH), list_dir(META_PATH))

#meta folder contains the text files - train.txt and test.txt
#train.txt contains the list of images that belong to training set
#test.txt contains the list of images that belong to test set
#classes.txt contains the list of all classes of food

# Helper method to split dataset into train and test folders
def prepare_data(filepath, src, dest):
    classes_images = defaultdict(list)
    with open(filepath, 'r') as txt:
        paths = [read.strip() for read in txt.readlines()]
        for p in paths:
            food = p.split('/')
            classes_images[food[0]].append(food[1] + '.jpg')

    for food in classes_images.keys():
        print("\nCopying images into ",food)
        if not os.path.exists(os.path.join(dest,food)):
            os.makedirs(os.path.join(dest,food))
        for i in classes_images[food]:
            copy(os.path.join(src,food,i), os.path.join(dest,food,i))
    print("Copying Done!")

# Prepare train dataset by copying images from food-101/images to food-101/train using the file train.txt
#print("Creating train data...")
#prepare_data(META_PATH+'train.txt', IMG_PATH, TRAIN_PATH)
# Prepare validation data by copying images from food-101/images to food-101/valid using the file test.txt
#print("Creating validation data...")
#prepare_data(META_PATH+'test.txt', IMG_PATH, VALID_PATH)

# Create Datasets & DataLoaders
train_ds, valid_ds, classes =  food.get_dataset()
num_classes = len(classes)
pp_(classes,num_classes)

train_dl, valid_dl = food.get_dls(train_ds, valid_ds, bs=bs, num_workers=2)

# Wrap all inside databunch
class DataBunch():
    def __init__(self, train_dl, valid_dl, c=None):
        self.train_dl, self.valid_dl, self.c = train_dl, valid_dl, c
    
    @property
    def train_ds(self): return self.train_dl.dataset
    
    @property
    def valid_ds(self): return self.valid_dl.dataset
    
    def __repr__(self):
        return str(self.__class__.__name__)+" obj (train & valid DataLoaders)"

data = DataBunch(train_dl, valid_dl, c=num_classes)
pp_(data.valid_ds, data.c, data.train_ds)

#Visualize random image from each of the 101 classes
# This can be used to print predictions too
def show_ds(trainset, classes, validset=None, cols=6, rows=17, preds=None, is_pred=False, is_valid=False):        
    fig = plt.figure(figsize=(25,25))
    fig.suptitle(f"Showing one random image from each {'Validation' if is_valid else 'Train'} classes", y=0.92, fontsize=24) # Adding  y=1.05, fontsize=24 helped me fix the suptitle overlapping with axes issue
    columns = cols
    rows = rows
    imgenet_mean = imagenet_stats[0]
    imgenet_std = imagenet_stats[1]  

    for i in range(1, columns*rows +1):
        fig.add_subplot(rows, columns, i)
        
        if is_pred and testset:
            img_xy = np.random.randint(len(testset));
            np_img = testset[img_xy][0].numpy()
            img = np.transpose(np_img, (1,2,0))            
            img = img * imgenet_std + imgenet_mean
        else:
            img_xy = np.random.randint(len(trainset));
            np_img = trainset[img_xy][0].numpy()
            img = np.transpose(np_img, (1,2,0))
            img = img * imgenet_std + imgenet_mean
        
        if is_pred:
            plt.title(classes[int(preds[img_xy])] + "/" + classes[testset[img_xy][1]])
        else:
            plt.title(classes[trainset[img_xy][1]])
        plt.axis('off')
        img = np.clip(img, 0, 1)
        plt.imshow(img, interpolation='nearest')
    plt.show()

#show_ds(data.train_ds, classes)
#show_ds(data.valid_ds, classes, is_valid=True)

# Training
loss_func=nn.CrossEntropyLoss()

def get_model(lr=3e-3,num_classes=num_classes, opt="SGD", **kwargs):
    #model_imgnet = models.resnet50(pretrained=True) # model
    #model_imgnet = models.resnet152(pretrained=True) # model
    #model_imgnet = models.wide_resnet50_2(pretrained=True) # model
    model_imgnet = models.wide_resnet101_2(pretrained=True) # model
    num_ftrs = model_imgnet.fc.in_features
    model_imgnet.fc = nn.Linear(in_features=num_ftrs, out_features=num_classes, bias=True)
    model = model_imgnet.to(device)
    print(model)
    if opt=="SGD":
        return model, optim.SGD(model.parameters(), lr=lr, **kwargs)
    elif opt=="Adam":
        return model, optim.Adam(model.parameters(), lr=lr, **kwargs)
    else:
        return model, optim.RMSprop(model.parameters(), lr=lr, **kwargs)

class Learner():
    """
    stores Model, Optimizer, Loss Function and Datasets (train & valid)
    """
    def __init__(self, model, opt, loss_func, data ):
        self.model, self.opt, self.loss_func, self.data = model, opt, loss_func, data

learn = Learner(*get_model(momentum=0.9, weight_decay=1e-4, nesterov=True), loss_func, data)

learn.loss_func = nn.CrossEntropyLoss()
learn.opt = torch.optim.SGD(learn.model.parameters(), lr=1e-4, momentum=0.95, weight_decay=1e-4)

pp_(learn.data, learn.loss_func, learn.opt) #, learn.model)

# Freezing the model (except some last layers)
# The approach is :
#first find the optimal Learnng rate for the freezed model using LR finder (cyclical learning rates) plot. Train the freezed pretrained model (e.g. resnet-50) for a few epochs with learning_rate= optimal L.R. (from lr finder) using 1-cycle-policy.
#next, unfreeze the model and find optimal learning rate using LR finder (cyclical learning rates) plot.
#finally train the unfreezed model using the optimal L.R. (found from L.R. finder) using 1-cycle-policy for some more epochs

freeze(learn.model)

#One Cycle Policy
#In paper (https://arxiv.org/pdf/1803.09820.pdf), author suggests to do one cycle during whole run with 2 steps of equal length. During first step, increase the learning rate from lower learning rate to higher learning rate. And in second step, decrease it from higher to lower learning rate. This is Cyclic learning rate policy. Author suggests one addition to this. - During last few hundred/thousand iterations of cycle reduce the learning rate to 1/100th or 1/1000th of the lower learning rate.
#Also, Author suggests that reducing momentum when learning rate is increasing. So, we make one cycle of momentum also with learning rate - Decrease momentum when learning rate is increasing and increase momentum when learning rate is decreasing.

class OneCycle(object):
    """
    In paper (https://arxiv.org/pdf/1803.09820.pdf), author suggests to do one cycle during 
    whole run with 2 steps of equal length. During first step, increase the learning rate 
    from lower learning rate to higher learning rate. And in second step, decrease it from 
    higher to lower learning rate. This is Cyclic learning rate policy. Author suggests one 
    addition to this. - During last few hundred/thousand iterations of cycle reduce the 
    learning rate to 1/100th or 1/1000th of the lower learning rate.

    Also, Author suggests that reducing momentum when learning rate is increasing. So, we make 
    one cycle of momentum also with learning rate - Decrease momentum when learning rate is 
    increasing and increase momentum when learning rate is decreasing.

    Args:
        nb              Total number of iterations including all epochs

        max_lr          The optimum learning rate. This learning rate will be used as highest 
                        learning rate. The learning rate will fluctuate between max_lr to
                        max_lr/div and then (max_lr/div)/div.

        momentum_vals   The maximum and minimum momentum values between which momentum will
                        fluctuate during cycle.
                        Default values are (0.95, 0.85)

        prcnt           The percentage of cycle length for which we annihilate learning rate
                        way below the lower learnig rate.
                        The default value is 10

        div             The division factor used to get lower boundary of learning rate. This
                        will be used with max_lr value to decide lower learning rate boundary.
                        This value is also used to decide how much we annihilate the learning 
                        rate below lower learning rate.
                        The default value is 10.
    """
    def __init__(self, nb, max_lr, momentum_vals=(0.95, 0.85), prcnt= 10 , div=10):
        self.nb = nb
        self.div = div
        self.step_len =  int(self.nb * (1- prcnt/100)/2)
        self.high_lr = max_lr
        self.low_mom = momentum_vals[1]
        self.high_mom = momentum_vals[0]
        self.prcnt = prcnt
        self.iteration = 0
        self.lrs = []
        self.moms = []
        
    def calc(self):
        self.iteration += 1
        lr = self.calc_lr()
        mom = self.calc_mom()
        return (lr, mom)
        
    def calc_lr(self):
        if self.iteration==self.nb:
            self.iteration = 0
            self.lrs.append(self.high_lr/self.div)
            return self.high_lr/self.div
        if self.iteration > 2 * self.step_len:
            ratio = (self.iteration - 2 * self.step_len) / (self.nb - 2 * self.step_len)
            lr = self.high_lr * ( 1 - 0.99 * ratio)/self.div
        elif self.iteration > self.step_len:
            ratio = 1- (self.iteration -self.step_len)/self.step_len
            lr = self.high_lr * (1 + ratio * (self.div - 1)) / self.div
        else :
            ratio = self.iteration/self.step_len
            lr = self.high_lr * (1 + ratio * (self.div - 1)) / self.div
        self.lrs.append(lr)
        return lr
    
    def calc_mom(self):
        if self.iteration==self.nb:
            self.iteration = 0
            self.moms.append(self.high_mom)
            return self.high_mom
        if self.iteration > 2 * self.step_len:
            mom = self.high_mom
        elif self.iteration > self.step_len:
            ratio = (self.iteration -self.step_len)/self.step_len
            mom = self.low_mom + ratio * (self.high_mom - self.low_mom)
        else :
            ratio = self.iteration/self.step_len
            mom = self.high_mom - ratio * (self.high_mom - self.low_mom)
        self.moms.append(mom)
        return mom

# Train for a few epochs(3 to 5) using CLR and 1-cycle-policy
total = 0
correct = 0

train_loss = 0
test_loss = 0
best_acc = 0
trn_losses = []
trn_accs = []
val_losses = []
val_accs = []

preds =[]

train_stats = AvgStats()
test_stats = AvgStats()

def train(epoch=0, use_cycle = False, model=learn.model):
    model.train()
    global best_acc
    global trn_accs, trn_losses
    is_improving = True
    counter = 0
    running_loss = 0.
    avg_beta = 0.98
    t1 = tqdm(learn.data.train_dl, leave=False, total=int(len(learn.data.train_dl)))
        
    for i, (input, target) in enumerate(t1):

        print("Iteration " + str(i+1))

        bt_start = time.time()
        if gpu :
            input, target = input.cuda(), target.cuda() 
        else :
            input, target = input.cpu(), target.cpu()
        var_ip, var_tg = Variable(input), Variable(target)
                
        if use_cycle:    
            lr, mom = onecycle.calc()
            update_lr(learn.opt, lr)
            update_mom(learn.opt, mom)
            
        output = model(var_ip)
        loss = learn.loss_func(output, var_tg)
            
        running_loss = avg_beta * running_loss + (1-avg_beta) *loss
        smoothed_loss = running_loss / (1 - avg_beta**(i+1))
        
        trn_losses.append(smoothed_loss)
            
        # measure accuracy and record loss
        prec = accuracy(output.data, target)
        trn_accs.append(prec)

        train_stats.append(smoothed_loss, prec, time.time()-bt_start)
        if prec > best_acc :
            best_acc = prec
            save_checkpoint(model, True)

        # compute gradient and do SGD step
        learn.opt.zero_grad()
        loss.backward()
        learn.opt.step()

def validate(model=learn.model):
    avg_beta = 0.98
    model.eval()
    global val_accs, val_losses
    running_loss = 0.
    t2 = tqdm(learn.data.valid_dl, leave=False, total=int(len(learn.data.valid_dl)))
    with torch.no_grad():
        for i, (input, target) in enumerate(t2):
            bt_start = time.time()
            if gpu :
                input, target = input.cuda(), target.cuda() 
            else :
                input, target = input.cpu(), target.cpu()
            var_ip, var_tg = Variable(input), Variable(target)
            output = model(var_ip)
            loss = learn.loss_func(output, var_tg)

            running_loss = avg_beta * running_loss + (1-avg_beta) *loss
            smoothed_loss = running_loss / (1 - avg_beta**(i+1))

            # measure accuracy and record loss
            prec = accuracy(output.data, target, is_test=True)
            test_stats.append(loss, prec, time.time()-bt_start)

            val_losses.append(smoothed_loss)
            val_accs.append(prec)

def fit(epochs, use_onecycle=False, model=learn.model):
    print(f"{'epoch':5s}{'train_loss':>15s}{'valid_loss':>15s}{'train_acc':>15s}{'valid_acc':>15s}")
    for epoch in tqdm(range(epochs), leave=False):
        train(epoch, use_onecycle, model)
        validate(model)
        print(f"{epoch+1:5}{trn_losses[-1]:15.5f}{val_losses[-1]:15.5f}{trn_accs[-1]:15.5f}{val_accs[-1]:15.5f}")

# Cyclic Learning Rates
# We'll use method described in paper : https://arxiv.org/abs/1506.01186 to find out optimum learning rate.
# We'll increase the learning rate from lower value per iteration for some iterations till loss starts exploding.
# We'll plot loss vs learning rate plot.
# We'll then select learning rate that is one power higher than the one where loss is minimum
# This value can be used as maximum value in Cyclic Learning Rate policy and minimum learning rate can be set to 1/3, 1/4th of max value.
# reference: https://github.com/nachiket273/One_Cycle_Policy

class CLR(object):
    """
    The method is described in paper : https://arxiv.org/abs/1506.01186 to find out optimum 
    learning rate. The learning rate is increased from lower value to higher per iteration 
    for some iterations till loss starts exploding.The learning rate one power lower than 
    the one where loss is minimum is chosen as optimum learning rate for training.

    Args:
        optim   Optimizer used in training.

        bn      Total number of iterations used for this test run.
                The learning rate increasing factor is calculated based on this 
                iteration number.

        base_lr The lower boundary for learning rate which will be used as
                initial learning rate during test run. It is adviced to start from
                small learning rate value like 1e-4.
                Default value is 1e-5

        max_lr  The upper boundary for learning rate. This value defines amplitude
                for learning rate increase(max_lr-base_lr). max_lr value may not be 
                reached in test run as loss may explode before reaching max_lr.
                It is adviced to use higher value like 10, 100.
                Default value is 100.

    """
    def __init__(self, learn, base_lr=1e-5, max_lr=100):
        self.base_lr = base_lr
        self.max_lr = max_lr
        self.optim = learn.opt
        self.bn = len(learn.data.train_dl) - 1
        ratio = self.max_lr/self.base_lr
        self.mult = ratio ** (1/self.bn)
        self.best_loss = 1e9
        self.iteration = 0
        self.lrs = []
        self.losses = []
        
    def calc_lr(self, loss):
        self.iteration +=1
        if math.isnan(loss) or loss > 4 * self.best_loss:
            return -1
        if loss < self.best_loss and self.iteration > 1:
            self.best_loss = loss
            
        mult = self.mult ** self.iteration
        lr = self.base_lr * mult
        
        self.lrs.append(lr)
        self.losses.append(loss)
        
        return lr
        
    def plot(self, start=10, end=-5):
        plt.xlabel("Learning Rate")
        plt.ylabel("Losses")
        plt.plot(self.lrs[start:end], self.losses[start:end])
        plt.xscale('log')
        plt.show()

def find_LR(clr):
    t = tqdm(learn.data.train_dl, leave=False, total=len(learn.data.train_dl))
    running_loss = 0.
    avg_beta = 0.98
    learn.model.train()
    for i, (input, target) in enumerate(t):
        if gpu :
            input, target = input.cuda(), target.cuda() 
        else :
            input, target = input.cpu(), target.cpu()
        var_ip, var_tg = Variable(input), Variable(target)
        output = learn.model(var_ip)
        loss = learn.loss_func(output, var_tg)

        running_loss = avg_beta * running_loss + (1-avg_beta) *loss
        smoothed_loss = running_loss / (1 - avg_beta**(i+1))
        t.set_postfix(loss=smoothed_loss)

        lr = clr.calc_lr(smoothed_loss)
        if lr == -1 :
            break
        update_lr(learn.opt, lr)   

        # compute gradient and do SGD step
        learn.opt.zero_grad()
        loss.backward()
        learn.opt.step()

        print('Iteration: {0}, Loss: {1}'.format(i, smoothed_loss))

# 1st LR find and train for few epochs
'''clr1 = CLR(learn)
find_LR(clr1)
save_model(learn.model, MODEL_PATH+"clr1.pth")
clr1.plot()'''

'''onecycle = OneCycle(calc_iters(learn.data.train_ds, epochs, bs), max_lr=2e-1, momentum_vals=(0.95, 0.8), prcnt=10)
fit(3, use_onecycle=True, model=learn.model)
save_model(learn.model, MODEL_PATH+'stage1_3.pth')
fit(3, use_onecycle=True, model=learn.model)
save_model(learn.model, MODEL_PATH+'stage1_6.pth')

# Returning to work
load_model(learn.model, MODEL_PATH+'stage1_6.pth')
onecycle = OneCycle(calc_iters(learn.data.train_ds, epochs, bs), max_lr=2e-1, momentum_vals=(0.95, 0.8), prcnt=10)
fit(2, use_onecycle=True, model=learn.model)
save_model(learn.model, MODEL_PATH+'stage1_8.pth')


# Unfreeze & train for longer epochs. (Works can be resumed from here). Now we will unfreeze the model and again find the optimal lr using Cyclic Learning Rates.
load_model(learn.model, MODEL_PATH+'stage1_8.pth')
unfreeze(learn.model)

#2nd LR Find: (Skip when doing for 2nd time)
clr2 = CLR(learn)
find_LR(clr2)
save_model(learn.model, MODEL_PATH+"clr2.pth")
clr.plot()'''

# Train the entire model for a few more epochs with the optimal max_lr found above (3e-4)
'''load_model(learn.model, MODEL_PATH+'stage1_8.pth')
onecycle = OneCycle(calc_iters(learn.data.train_ds, epochs, bs), max_lr=1e-3, momentum_vals=(0.95, 0.8), prcnt=10)
fit(10, use_onecycle=True, model=learn.model)
save_model(learn.model, MODEL_PATH+"stage2_10-test.pth")

# Train some more epochs
onecycle = OneCycle(calc_iters(learn.data.train_ds, epochs, bs), max_lr=1e-3, momentum_vals=(0.95, 0.8), prcnt=10)
fit(10, use_onecycle=True, model=learn.model)
save_model(learn.model, MODEL_PATH+"stage2_20.pth")'''

load_model(learn.model, MODEL_PATH+"stage2_35.pth")
onecycle = OneCycle(calc_iters(learn.data.train_ds, epochs, bs), max_lr=3e-3, momentum_vals=(0.95, 0.8), prcnt=10)
fit(15, use_onecycle=True, model=learn.model)
save_model(learn.model, MODEL_PATH+"stage2_50.pth")

# Visualizations
plt.rcParams['figure.figsize'] = (5, 4)
plt.xlabel("Iterations")
plt.ylabel("Accuracy")
plt.yticks(np.arange(0, 100, step=5))
plt.plot(train_stats.precs, 'go-', label='Accuracy')
plt.legend()
plt.xticks(np.arange(0, len(train_stats.precs), step=2000), rotation=60)
plt.savefig('onecycle_acc.jpg')

ep_losses = []
for i in range(0, len(train_stats.losses), len(learn.data.train_dl)):
    if i != 0 :
        ep_losses.append(train_stats.losses[i])
        
ep_lossesv = []
for i in range(0, len(test_stats.losses), len(learn.data.valid_dl)):
    if(i != 0):
        ep_lossesv.append(test_stats.losses[i])

plt.xlabel("Epochs")
plt.ylabel("Loss %")
plt.xticks(np.arange(0, 1000, step=10))
plt.plot(ep_losses, 'r', label='Train')
plt.plot(ep_lossesv, 'b', label='Val')
plt.legend()
plt.savefig('onecycle_loss.jpg')

plt.xlabel("Iterations")
plt.ylabel("Learning Rate")
plt.xticks(np.arange(0, len(onecycle.lrs), step=1000), rotation=90)
plt.plot(onecycle.lrs[:3500])
plt.savefig('onecycle_lr.jpg')

plt.xlabel("Iterations")
plt.ylabel("Momentum")
plt.xticks(np.arange(0, len(onecycle.moms), step=1000), rotation=90)
plt.plot(onecycle.moms[:3500])
plt.savefig('onecycle_mom.jpg')
